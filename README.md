# CMS DT Dashboard

https://cms-dt-dashboard.app.cern.ch/

## Development

### Compile and run

Most of the time, just `OMS_CLIENT_ID=<THE_CLIENT_ID> OMS_CLIENT_SECRET=<IT_IS_A_SECRET> cargo run` will do the trick, even though, `OMS_CLIENT_ID=<THE_CLIENT_ID> OMS_CLIENT_SECRET=<IT_IS_A_SECRET> cargo watch -w src -w templates -w assets -x run` is a more ergonomic option.

If compilation goes fine, you can access [http://localhost:8000/](http://localhost:8000/).

N.B.: You might need to setup a tunnel and forward the port 8000. This can be accomplished by: 

```bash 
ssh -L 8000:localhost:8000 dev-username@development-server
```

### Deployment

Just push the updated code to the `master` branch on Gitlab. The CI pipeline for `push` has to be done manually (https://gitlab.cern.ch/ftorresd/cms-dt-dashboard/-/pipelines).

If all pipelines finish succesfully, you should update the PaaS Image Stream.

At `lxplus`:

```
# You need to login on OpenShift. Copy the command from:
# https://oauth-openshift.paas.cern.ch/oauth/token/request
oc project cms-dt-dashboard
oc import-image gitlab-registry.cern.ch/ftorresd/cms-dt-dashboard --all
```

This will automatically trigger the re-deployment of another application pods.
