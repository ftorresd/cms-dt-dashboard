use crate::app_state::SharedAppState;
use reqwest::{ClientBuilder, StatusCode};
use serde::Deserialize;
use serde_json::Value;
use std::collections::HashMap;
use std::error::Error;
use std::str::FromStr;
use std::sync::Arc;
use tokio::task::JoinSet;
use tokio::time::{sleep, Duration};

#[derive(Clone)]
pub enum OMSQueryErrors {
    MaxAuthTrialsReached,
    BadHttpStatus(u16),
    BadMetaData,
    BadResourceCount,
    CanNotJoinQueries,
    BadResponse,
    EmptyEndPoint,
    BadFilterOperator,
}

impl std::fmt::Display for OMSQueryErrors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::MaxAuthTrialsReached => write!(f, "Max auth trials reached"),
            Self::BadHttpStatus(s) => write!(f, "Returned status code: {}", s),
            Self::BadMetaData => write!(f, "Could not query meta data."),
            Self::BadResourceCount => write!(f, "Could not estimate the query total size."),
            Self::CanNotJoinQueries => write!(f, "Failed to join OMS queries."),
            Self::BadResponse => write!(f, "One of the queries failed."),
            Self::EmptyEndPoint => write!(f, "No EndPoint was defined."),
            Self::BadFilterOperator => write!(f, "Requested filter operator is not defined."),
        }
    }
}

impl std::fmt::Debug for OMSQueryErrors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::MaxAuthTrialsReached => write!(f, "Max auth trials reached"),
            Self::BadHttpStatus(s) => write!(f, "Returned status code: {}", s),
            Self::BadMetaData => write!(f, "Could not query meta data."),
            Self::BadResourceCount => write!(f, "Could not estimate the query total size."),
            Self::CanNotJoinQueries => write!(f, "Failed to join OMS queries."),
            Self::BadResponse => write!(f, "One of the queries failed."),
            Self::EmptyEndPoint => write!(f, "No EndPoint was defined."),
            Self::BadFilterOperator => write!(f, "Requested filter operator is not defined."),
        }
    }
}

impl Error for OMSQueryErrors {}
type Result<T> = std::result::Result<T, OMSQueryErrors>;

#[derive(Debug, PartialEq)]
pub enum OMSFilterOperators {
    EQ,
    NEQ,
    GT,
    GE,
    LT,
    LE,
    LIKE,
}

impl FromStr for OMSFilterOperators {
    type Err = OMSQueryErrors;

    fn from_str(input: &str) -> Result<OMSFilterOperators> {
        match input {
            "EQ" => Ok(OMSFilterOperators::EQ),
            "NEQ" => Ok(OMSFilterOperators::NEQ),
            "GT" => Ok(OMSFilterOperators::GT),
            "GE" => Ok(OMSFilterOperators::GE),
            "LT" => Ok(OMSFilterOperators::LT),
            "LE" => Ok(OMSFilterOperators::LE),
            "LIKE" => Ok(OMSFilterOperators::LIKE),
            _ => Err(OMSQueryErrors::BadFilterOperator),
        }
    }
}

impl std::fmt::Display for OMSFilterOperators {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::EQ => write!(f, "EQ"),
            Self::NEQ => write!(f, "NEQ"),
            Self::GT => write!(f, "GT"),
            Self::GE => write!(f, "GE"),
            Self::LT => write!(f, "LT"),
            Self::LE => write!(f, "LE"),
            Self::LIKE => write!(f, "LIKE"),
        }
    }
}

// #[derive(Deserialize, Debug, Clone, Default)]
// pub struct OMSQueryResponseLinks {
//     pub first: Option<String>,
//     pub last: Option<String>,
//     pub next: Option<String>,
//     pub prev: Option<String>,
// }

/// Reference: https://twiki.cern.ch/twiki/bin/viewauth/CMS/OMSRestfulAPI
#[derive(Deserialize, Debug, Clone, Default)]
pub struct OMSQueryResponse {
    pub data: Vec<Value>,
    // pub links: OMSQueryResponseLinks,
    pub meta: Value,
}

#[derive(Debug, Clone)]
pub struct OMSQueryClient {
    pub endpoint: Option<String>,
    pub agg: String,
    pub params: HashMap<String, String>,
}

impl Default for OMSQueryClient {
    fn default() -> Self {
        Self {
            endpoint: None,
            agg: "agg".to_string(),
            params: HashMap::<String, String>::new(),
        }
    }
}

impl OMSQueryClient {
    // pub fn new() -> Self {
    //     Self::default()
    // }

    // https://cmsoms.cern.ch/dt-agg/api/v1/dt/dtsuperlayerhvdpidmaps/alll3_cathodes_dpids?filter[wheel][EQ]=-2&filter[station][EQ]=1&filter[sector][EQ]=1&filter[superlayer][EQ]=1&filter[superlayer_name][EQ]=WM2_MB1_S01_SL1&filter[l1_wires0_dpid][EQ]=9544&filter[l1_wires1_dpid][EQ]=9545&filter[l1_strips_dpid][EQ]=9546&filter[l1_cathodes_dpid][EQ]=9547&filter[l2_wires0_dpid][EQ]=9549&filter[l2_wires1_dpid][EQ]=9550&filter[l2_strips_dpid][EQ]=9551&filter[l2_cathodes_dpid][EQ]=9552&filter[l3_wires0_dpid][EQ]=9554&filter[l3_wires1_dpid][EQ]=9555&filter[l3_strips_dpid][EQ]=9556&sort=l3_cathodes_dpid00
    pub fn from_endpoint(endpoint: &str) -> Self {
        let mut this_client = OMSQueryClient::default();
        this_client.endpoint = Some(endpoint.to_string());

        this_client
    }

    pub fn aggregation(mut self, agg: &str) -> Self {
        self.agg = agg.into();
        self
    }

    pub fn with_param(mut self, param: &str, value: &str) -> Self {
        self.params.insert(param.to_string(), value.to_string());
        self
    }

    pub fn filter(mut self, param: &str, operator: OMSFilterOperators, value: &str) -> Self {
        self.params.insert(
            format!("filter[{}][{}]", param, operator.to_string()),
            value.to_string(),
        );
        self
    }

    pub fn project_on(mut self, param: &str) -> Self {
        let projection_key: &str = "fields";

        self.params.insert(
            projection_key.to_string(),
            match self.params.get(projection_key) {
                Some(old_projection) => {
                    format!("{},{}", old_projection, param)
                }
                None => param.to_string(),
            },
        );

        self
    }

    pub fn sort_by(mut self, field: &str) -> Self {
        match self.params.get("sort") {
            Some(old_sort) => {
                self.params
                    .insert("sort".to_string(), format!("{},{}", old_sort, field));
            }
            None => {
                self.params.insert("sort".to_string(), field.to_string());
            }
        }

        self
    }

    pub fn include(mut self, param: &str) -> Self {
        match self.params.get("include") {
            Some(old_include) => {
                self.params
                    .insert("include".to_string(), format!("{},{}", old_include, param));
            }
            None => {
                self.params.insert("include".to_string(), param.to_string());
            }
        }

        self
    }

    pub fn include_meta(self) -> Self {
        self.include("meta")
    }

    fn collect_params(&self) -> Result<String> {
        let mut params = String::new();

        if !self.params.is_empty() {
            for (k, v) in self.params.iter() {
                if k != "page[offset]" && k != "page[limit]" {
                    params.push_str("&");
                    params.push_str(&k);
                    params.push_str("=");
                    params.push_str(&v);
                }
            }
        }
        Ok(params)
    }

    fn make_url(&self, offset: i32, limit: i32) -> Result<String> {
        // "https://cmsoms.cern.ch/agg/api/v1/eras?page[offset]=0&page[limit]=10"
        // "https://cmsoms.cern.ch/agg/api/v1/eras?include=meta"
        // "https://cmsoms.cern.ch/agg/api/v1/eras/meta"
        // "https://cmsoms.cern.ch/agg/api/v1/fills?fields=fill_number,first_run_number,last_run_number,duration,start_time,start_stable_beam,end_stable_beam,end_time,delivered_lumi,recorded_lumi,efficiency_time,efficiency_lumi,peak_lumi,peak_pileup,bunches_colliding,beta_star,energy,injection_scheme&page[offset]=0&page[limit]=100&filter[fill_type_runtime][EQ]=PROTONS&filter[stable_beams][EQ]=true&filter[start_time][GE]=2022-01-01&filter[end_time][LE]=2025-12-31&sort=-fill_number&include=meta,presentation_timestamp"

        // let endpoint: &str = &self.endpoint.ok_or_else(|| OMSQueryErrors::EmptyEndPoint)?;
        let url = format!(
            "https://cmsoms.cern.ch/{}/api/v1/{}?page[offset]={}&page[limit]={}&include=meta{}",
            self.agg,
            &self
                .endpoint
                .clone()
                .ok_or_else(|| OMSQueryErrors::EmptyEndPoint)?,
            offset,
            limit,
            self.collect_params()?
        );

        #[cfg(debug_assertions)]
        log::debug!("Will call URL: {}", url);

        Ok(url)
    }

    async fn send_request(&self, url: &str, access_token: &str) -> (StatusCode, OMSQueryResponse) {
        let resp = ClientBuilder::new()
            .danger_accept_invalid_certs(true)
            .build()
            .expect("[from send_request] Could not build reqwest client")
            .get(url)
            .bearer_auth(access_token)
            .send()
            .await
            .expect("[from send_request] Could not send OMS request");

        if resp.status() != reqwest::StatusCode::OK {
            log::warn!(
                "OMS Query request status is: {}. Application may terminate.",
                &resp.status()
            )
        }

        match resp.status() {
            StatusCode::OK => (
                resp.status(),
                resp.json::<OMSQueryResponse>()
                    .await
                    .expect("[from send_request] Could not deserialize response."),
            ),
            _ => (resp.status(), OMSQueryResponse::default()),
        }
    }

    async fn do_query(
        &self,
        offset: i32,
        limit: i32,
        is_meta: bool,
        app_state: &SharedAppState,
    ) -> Result<OMSQueryResponse> {
        // validation
        match self.endpoint {
            None => return Err(OMSQueryErrors::EmptyEndPoint),
            _ => {}
        }

        const MAX_REQUEST_TRIALS: i32 = 3;
        const SLEEPING_TIME: u64 = 0;
        let mut trial = 1;

        // sanity checks
        if is_meta {
            assert_eq!(offset, 0);
            assert_eq!(limit, 1);
        }

        loop {
            let app_state_guard = app_state.read().await;
            let (status, resp) = self
                .send_request(
                    &self.make_url(offset, limit)?,
                    &(*app_state_guard).oms_auth.access_token,
                )
                .await;
            drop(app_state_guard);

            match status {
                reqwest::StatusCode::OK => {
                    return Ok(resp);
                }
                reqwest::StatusCode::UNAUTHORIZED => {
                    if is_meta {
                        // renew OMS access token
                        let mut app_state_guard = app_state.write().await;
                        let _ = &(*app_state_guard).oms_auth.renew_access_token().await;
                        drop(app_state_guard);
                    }
                }
                _ => {
                    return Err(OMSQueryErrors::BadHttpStatus(status.as_u16()));
                }
            }

            if trial > MAX_REQUEST_TRIALS {
                return Err(OMSQueryErrors::MaxAuthTrialsReached);
            }

            log::info!("Trial {} not successful", trial);
            trial += 1;

            // sleep before try again
            if SLEEPING_TIME > 0 {
                sleep(Duration::from_millis(SLEEPING_TIME)).await;
            }
        }
    }

    pub async fn query_meta(&self, app_state: &SharedAppState) -> Result<OMSQueryResponse> {
        self.do_query(0, 1, true, app_state).await
    }

    fn n_pages(entries: i32, limit: i32) -> i32 {
        entries / limit + (entries % limit > 0) as i32
    }

    pub async fn query(&self, app_state: &SharedAppState) -> Result<Vec<OMSQueryResponse>> {
        // const PAGE_LIMIT: i32 = 100_000;
        const PAGE_LIMIT: i32 = 50_000;

        let meta_resp = self.query_meta(app_state).await?;

        let total_resource_count = meta_resp.meta["totalResourceCount"]
            .as_i64()
            .ok_or_else(|| OMSQueryErrors::BadResourceCount)?
            as i32;
        let number_of_pages = Self::n_pages(total_resource_count, PAGE_LIMIT);

        #[cfg(debug_assertions)]
        log::debug!("[PAGINATION] Number of pages: {}", number_of_pages);

        // build data requests
        let mut set = JoinSet::new();
        for i in 0..number_of_pages {
            let shared_app_state = app_state.clone();
            let cloned_self = Arc::new(self.clone());
            set.spawn(async move {
                sleep(Duration::from_millis(i as u64 / 10 * 500)).await;
                (
                    i as usize,
                    cloned_self
                        .do_query(i * PAGE_LIMIT, PAGE_LIMIT, false, &shared_app_state)
                        .await,
                )
            });
        }

        #[cfg(debug_assertions)]
        log::debug!("Joinning Results ...");

        let mut data_resps = vec![OMSQueryResponse::default(); number_of_pages as usize];
        while let Some(res) = set.join_next().await {
            let (idx, resp) = res.map_err(|_| OMSQueryErrors::CanNotJoinQueries)?;
            data_resps[idx] = resp?;
        }

        Ok(data_resps)
    }
}
