use chrono::{offset, DateTime};
use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct OMSAuthResponse {
    access_token: String,
}

#[derive(Debug)]
pub struct OMSAuth {
    id: String,
    secret: String,
    timestamp: DateTime<offset::Utc>,
    pub access_token: String,
}

impl OMSAuth {
    pub async fn new(id: &str, secret: &str) -> Self {
        let mut this_auth = Self {
            id: id.to_string(),
            secret: secret.to_string(),
            timestamp: offset::Utc::now(),
            access_token: String::from(""),
        };

        this_auth.get_access_token().await;

        this_auth
    }

    pub async fn get_access_token(&mut self) {
        let body = format!(
            "grant_type=client_credentials&client_id={}&client_secret={}&audience=cmsoms-prod",
            self.id, self.secret
        );

        let access_token_resp = reqwest::ClientBuilder::new()
            // needed since we run on SCRATCH - should update?!
            .danger_accept_invalid_certs(true)
            .build()
            .expect("[from get_access_token] Could not create reqwest client")
            .post("https://auth.cern.ch/auth/realms/cern/api-access/token")
            .body(body)
            .header(
                reqwest::header::CONTENT_TYPE,
                "application/x-www-form-urlencoded",
            )
            .send()
            .await
            .expect("[from get_access_token] Could not authenticate client");
        if access_token_resp.status() != reqwest::StatusCode::OK {
            log::error!(
                "OMS Access Token request status is: {}. Application will terminate.",
                access_token_resp.status()
            )
        }

        self.timestamp = offset::Utc::now();
        self.access_token = access_token_resp
            .json::<OMSAuthResponse>()
            .await
            .expect("[from get_access_token] Could not get Access Token.")
            .access_token;
    }

    pub async fn renew_access_token(&mut self) {
        if (offset::Utc::now() - self.timestamp).num_minutes() >2 {
            self.get_access_token().await;
        }
    }
}
