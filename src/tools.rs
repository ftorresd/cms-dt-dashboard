use handlebars::{no_escape, Handlebars};
use std::collections::HashMap;

pub fn render(template: &str, data: &HashMap<&str, &str>) -> String {
    let mut render = Handlebars::new();
    render.register_escape_fn(no_escape);
    render.render_template(template, &data).unwrap()
}
