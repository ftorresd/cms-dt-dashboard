use axum::body::Body;
use axum::http::Request;
use axum::{
    extract,
    http::StatusCode,
    response::{Html, IntoResponse, Redirect},
    routing::get,
    Router,
};
use axum_extra::routing::RouterExt;
use axum_macros::debug_handler;
use serde::Deserialize;
use simple_logger::SimpleLogger;
use std::collections::HashMap;
use std::env;
use std::sync::Arc;
use tokio::sync::RwLock;
use tower_http::services::ServeDir;
use tracing::Level;
use tracing_subscriber;

// custom
pub mod oms_auth;

pub mod tools;

pub mod settings_menu;
use settings_menu::SettingsMenu;

pub mod oms_query;
use oms_query::{OMSFilterOperators, OMSQueryClient};

pub mod app_state;
use app_state::{AppState, SharedAppState};

#[derive(Deserialize, Debug)]
struct ChannelQuery {
    channel: String,
    run_range: String,
    time_range: String,
}

#[tokio::main]
async fn main() {
    // logger
    SimpleLogger::new()
        .init()
        .expect("Could not start logger (0_o) ");

    // tracing configs
    // Build the subscriber
    // use that subscriber to process traces emitted after this point
    tracing::subscriber::set_global_default(
        tracing_subscriber::fmt()
            // Use a more compact, abbreviated log format
            .compact()
            .with_max_level(Level::INFO)
            .finish(),
    )
    .expect("Could not set global tracing subscriber");

    // create application state
    let app_state = AppState::new(
        env::var("SERVER_HOSTNAME").unwrap_or("localhost".to_string()),
        env::var("SERVER_PORT").unwrap_or("8000".to_string()),
        env::var("OMS_CLIENT_ID").expect("ERROR: OMS_CLIENT_ID not set"),
        env::var("OMS_CLIENT_SECRET").expect("ERROR: OMS_CLIENT_SECRET not set"),
    )
    .await;

    // let shared_state = Arc::new(app_state);
    let shared_state = Arc::new(RwLock::new(app_state));

    // build our application with a route
    let app = Router::new()
        .route("/", get(root))
        .route_with_tsr("/summary", get(summary))
        .route_with_tsr("/hv", get(hv))
        .route_with_tsr("/inner_hv", get(inner_hv))
        .route_with_tsr("/lv", get(lv))
        .route_with_tsr("/gas", get(gas))
        .route_with_tsr("/environmental", get(environmental))
        .nest_service("/assets", ServeDir::new("assets"))
        .fallback(handler_404)
        .with_state(shared_state.clone());

    // listen
    let listener = tokio::net::TcpListener::bind(format!("{}:{}", "0.0.0.0", "8000"))
        .await
        .unwrap();
    log::info!("Listening on {}", listener.local_addr().unwrap());

    // serve
    axum::serve(listener, app).await.unwrap();
}

async fn handler_404() -> impl IntoResponse {
    (
        StatusCode::NOT_FOUND,
        Html(include_str!("../templates/404.html")),
    )
}

async fn root() -> Redirect {
    Redirect::permanent("/summary")
}

async fn base_handler(
    extract::State(app_state): extract::State<SharedAppState>,
    req: Request<Body>,
    page_title: &str,
    menu_type: SettingsMenu,
    inner_request: &str,
) -> impl IntoResponse {
    let mut data = HashMap::new();

    let app_state_guard = app_state.read().await;
    let url_debug = format!(
        "//{}:{}",
        &*app_state_guard.hostname, &*app_state_guard.port
    );
    let url_prod = format!("//{}", &*app_state_guard.hostname);

    if String::from(&app_state_guard.hostname) == "localhost" {
        data.insert("url", url_debug.as_ref());
    } else {
        data.insert("url", url_prod.as_ref());
    }
    drop(app_state_guard);

    data.insert("PAGE_TITLE", page_title);
    data.insert("SETTINGS_MENU", SettingsMenu::build_menu(menu_type));

    // set inner request path
    let mut query_string = String::new();
    match req.uri().query() {
        Some(query) => {
            query_string = format!("/{}?{}", inner_request, query);
            data.insert("INNER_REQUEST", query_string.as_ref());
        }
        None => {
            query_string = format!("/{}", inner_request);
            data.insert("INNER_REQUEST", query_string.as_ref());
        }
    }

    (
        StatusCode::from_u16(200).expect("[from handle] Wrong status code."),
        Html(tools::render(
            include_str!("../templates/dt-dashboard.html"),
            &data,
        )),
    )
}

#[debug_handler]
async fn summary(
    extract::State(app_state): extract::State<SharedAppState>,
    req: Request<Body>,
) -> impl IntoResponse {
    base_handler(
        extract::State(app_state),
        req,
        "Summary",
        SettingsMenu::Summary,
        "assets/not-implemented.html",
    )
    .await
}

#[debug_handler]
async fn hv(
    extract::State(app_state): extract::State<SharedAppState>,
    req: Request<Body>,
) -> impl IntoResponse {
    base_handler(
        extract::State(app_state),
        req,
        "HV",
        SettingsMenu::Channel,
        "inner_hv",
    )
    .await
}

#[debug_handler]
async fn lv(
    extract::State(app_state): extract::State<SharedAppState>,
    req: Request<Body>,
) -> impl IntoResponse {
    base_handler(
        extract::State(app_state),
        req,
        "LV",
        SettingsMenu::Channel,
        "assets/not-implemented.html",
    )
    .await
}

#[debug_handler]
async fn gas(
    extract::State(app_state): extract::State<SharedAppState>,
    req: Request<Body>,
) -> impl IntoResponse {
    base_handler(
        extract::State(app_state),
        req,
        "Gas",
        SettingsMenu::Channel,
        "assets/not-implemented.html",
    )
    .await
}

#[debug_handler]
async fn environmental(
    extract::State(app_state): extract::State<SharedAppState>,
    req: Request<Body>,
) -> impl IntoResponse {
    base_handler(
        extract::State(app_state),
        req,
        "Environmental",
        SettingsMenu::Channel,
        "assets/not-implemented.html",
    )
    .await
}

#[debug_handler]
async fn inner_hv(
    extract::State(app_state): extract::State<SharedAppState>,
    query: Option<extract::Query<ChannelQuery>>,
) -> impl IntoResponse {
    match &query {
        Some(query) => {
            // let my_query = OMSQueryClient::from_endpoint("fills")
            // let my_query = OMSQueryClient::from_endpoint("runs")
            //     .project_on("run_number")
            //     .project_on("start_time")
            //     .project_on("end_time")
            //     .filter("run_number", OMSFilterOperators::NEQ, "null")
            //     .filter("start_time", OMSFilterOperators::NEQ, "null")
            //     .filter("end_time", OMSFilterOperators::NEQ, "null")
            //     .query(&app_state)
            //     .await
            //     .unwrap();

            // https://cmsoms.cern.ch/dt-agg/api/v1/dt/dthvsl1views?fields=change_date,imon&page[offset]=0&page[limit]=10000&filter[imon][NEQ]=NULL&filter[dpid][EQ]=9552&filter[change_date][GE]=2010-08-14T23%3A59%3A59Z&filter[change_date][LE]=2024-01-04T15%3A15%3A41Z&sort=change_date&include=meta

            let my_query = OMSQueryClient::from_endpoint("dt/dthvsl1views")
                .aggregation("dt-agg")
                .project_on("change_date")
                .project_on("imon")
                .project_on("vmon")
                // .filter("imon", OMSFilterOperators::NEQ, "NULL")
                // .filter("vmon", OMSFilterOperators::NEQ, "NULL")
                .filter("dpid", OMSFilterOperators::EQ, "9552")
                .filter(
                    "change_date",
                    OMSFilterOperators::GE,
                    "2010-08-14T23%3A59%3A59Z",
                )
                .filter("change_date", OMSFilterOperators::LE, "2024-01-04")
                .sort_by("change_date")
                .query(&app_state)
                .await
                .unwrap();

            let mut data = HashMap::new();
            let mut count: usize = 0;
            for resp in my_query {
                count += resp.data.len();
                for _data in resp.data {
                    // println!("----- Data: {}", data.to_string());
                }
            }
            // println!("----- Counts: {}", count);
            // data.insert("OMS_DATA", my_query.data.to_string());

            data.insert("OMS_DATA", "Hello from OMS");

            (
                StatusCode::from_u16(200).expect("[from handle] Wrong status code."),
                Html(format!(
                    "{}",
                    tools::render(include_str!("../templates/inner-hv.html"), &data)
                )),
            )
        }
        None => (
            StatusCode::from_u16(200).expect("[from handle] Wrong status code."),
            Html("".to_string()),
        ),
    }
}
