pub enum SettingsMenu {
    Summary,
    Channel,
}

impl SettingsMenu {
    pub fn build_menu(menu_type: Self) -> &'static str {
        match menu_type {
            Self::Summary => include_str!("../templates/settings-summary.html"),
            Self::Channel => include_str!("../templates/settings-channel.html"),
        }
    }
}
