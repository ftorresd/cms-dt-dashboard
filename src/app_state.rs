use crate::oms_auth::OMSAuth;
use std::sync::Arc;
use tokio::sync::RwLock;

#[derive(Debug, )]
pub struct AppState {
    pub hostname: String,
    pub port: String,
    pub oms_auth: OMSAuth,
}

impl AppState {
    pub async fn new(hostname: String, port: String, client_id: String, secret: String) -> Self {
        AppState {
            hostname: hostname,
            port: port,
            oms_auth: OMSAuth::new(&client_id, &secret).await,
        }
    }
}
pub type SharedAppState = Arc<RwLock<AppState>>;
